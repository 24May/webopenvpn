<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'create', 'update', 'delete', 'admin', 'error_generate'),
                'roles'=>array('1'),
            ),
            array('deny',  // deny all users
                'roles'=>array('0', 'guest'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save()) {

                //  Generate PDF instruction
                $count_ch = 10; // количество сиволов в пароле
                $var1 = '';
                $accepted = 'zyxwvutsrqponmlkjihgfedcba0987654321QWERTYUIOPASDFGHJKLMNBVCXZ';  // доступный набор символов в пароле
                srand(((int)((double)microtime()*1000000))); // меняем начальное число генератора случайных чисел
                for ($i=0; $i<=$count_ch; $i++) { //формируем случайный символ для каждой составляющей пароля
                    $random = rand(0, (strlen($accepted) -1)); // берем случайный индекс из строки $accepted
                    $var1 .= $accepted[$random] ; // дописываем в конец генерируемого пароля полученный символ
                }

                $count_ch = 15; // количество сиволов в пароле
                $var2 = '';
                //$accepted = 'zyxwvutsrqponmlkjihgfedcba0987654321QWERTYUIOPASDFGHJKLMNBVCXZ';  // доступный набор символов в пароле
                srand(((int)((double)microtime()*1000000))); // меняем начальное число генератора случайных чисел
                for ($i=0; $i<=$count_ch; $i++) { //формируем случайный символ для каждой составляющей пароля
                    $random = rand(0, (strlen($accepted) -1)); // берем случайный индекс из строки $accepted
                    $var2 .= $accepted[$random] ; // дописываем в конец генерируемого пароля полученный символ
                }

                //$var1 = '1234567';
                $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                    'P', 'cm', 'A4', true, 'UTF-8');
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor("Andrey Kopitsa");
                $pdf->SetTitle("Vpn Access");
                $pdf->SetSubject("Vpn Access");
                $pdf->SetKeywords("Vpn Access");
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->getAliasNbPages();

                $pdf->AddPage();
                // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
                if($model->id_company == 1) {
                    $pdf->Image($_SERVER['DOCUMENT_ROOT'] . 'files/logo-flex.gif', 1, 1, 4, 1.5, 'GIF', 'http://www.flexaspect.com', '', true, '300', '', false, false, 0, false, false, false);
                } else {
                    $pdf->Image($_SERVER['DOCUMENT_ROOT'] . 'files/logo-ukad.gif', 1, 1, 4, 1, 'GIF', 'http://ukad-group.com', '', true, '300', '', false, false, 0, false, false, false);
                }
                //$pdf->SetFont("times", "BI", 20);
                $pdf->SetFont('freeserif', '', 12);
                //$pdf->Cell(0,10,"Example 002",1,1,'C');
                $data = '<div style="padding-top: 90px;"><h1 style="color: #880000; text-align: center;">OpenVPN Access</h1>
<table style="padding: 5px;" border="1"><tbody><tr bgcolor="#ccc"><th width="30%"><strong>Клиент</strong></th>
<th width="70%"><strong>Инструкция</strong></th></tr>
<tr><td><strong>Имя:</strong>' . $model->name . '<br><strong>Фамилия:</strong>'.$model->surname.'<br /><strong>Пароль к VPN: </strong>'.$var2.'</td><td>
<ol>
<li>Для Windows скачать последнюю версию клиента Openvpn по ссылке <a href="https://openvpn.net/index.php/open-source/downloads.html">https://openvpn.net/index.php/open-source/downloads.html</a></li>
<li>Установить программу со значениями по-умолчанию, подтверждая все предлагаемые действия</li>
<li>Распаковать все файлы из архива в папку с программой в папку C:/Program Files/OpenVPN/config</li>
<li>Запустить программу OpenVPN GUI с правами администратора</li>
<li>В трее кликнуть правой кнопкой по иконке, выбрать необходимое соединение.</li>
<li>При выборе <strong>uadevelopers</strong> выход интернет будет через вашего провайдера, а при <strong>uadevelopers-gateway</strong> через шлюз офиса</li>
<li>При запросе пароля ввести пароль с PDF инструкции</li>
<li><strong>Домашняя сеть не должна совпадать с офисной, т.е. не должны быть 192.168.0.0/24 и 192.168.4.0/24</strong></li>
</ol>


</td></tr></tbody></table></div>';
                $pdf->writeHTML($data, true, false, false, false, '');
                //for show in browser
                //$pdf->Output("vpn-access.pdf", "I");
                $pdf->SetProtection(array('print'), $var1);
                $pdf->Output($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.pdf', 'F');

                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.pdf')) {
                    Yii::app()->user->setFlash('tipPdf','Проблема с генерацией PDF');
                    $this->redirect('error_generate');
                }


                //  Generate PDF instruction

                // Create Config File
                $vpn_conf = <<<EOD
dev tun
proto tcp
remote 82.117.232.116
remote 82.207.43.250
port 2001
client
resolv-retry infinite
ca ca.crt
cert $model->login.crt
key $model->login.key
tls-auth ta.key 1
auth SHA1
cipher AES-256-CBC
ns-cert-type server
comp-lzo
persist-key
persist-tun
verb 3
route-method exe
route-delay 2
keepalive 5 30

EOD;
                $vpn1_conf = <<<EOD
dev tun
proto tcp
remote 82.117.232.116
remote 82.207.43.250
port 2002
client
resolv-retry infinite
ca ca.crt
cert $model->login.crt
key $model->login.key
tls-auth ta.key 1
auth SHA1
cipher AES-256-CBC
ns-cert-type server
comp-lzo
persist-key
persist-tun
verb 3
route-method exe
route-delay 2
keepalive 5 30

EOD;


                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers.ovpn', 'w' );
                flock($fp, LOCK_EX);
                fwrite($fp, $vpn_conf);
                flock($fp, LOCK_UN);
                fclose($fp);

                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers-gateway.ovpn', 'w' );
                flock($fp, LOCK_EX);
                fwrite($fp, $vpn1_conf);
                flock($fp, LOCK_UN);
                fclose($fp);

                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.pdf')) {
                    Yii::app()->user->setFlash('tipOvpn','Проблема с созданием конфигурационного файла');
                    $this->redirect('error_generate');
                }

                $run_create_cert = "sudo /etc/openvpn/easy-rsa/run.sh"." ".$model->login." ".$var2;
                $result = system($run_create_cert, $retval);

                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.key') || !file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.crt') || !file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' .'ta.key') || !file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/' .'ca.crt')) {
                    Yii::app()->user->setFlash('tipCert','Проблема с созданием сертификатов');
                    $this->redirect('error_generate');
                }

                $zip = new ZipArchive;
                $filename = $_SERVER['DOCUMENT_ROOT'] . 'files/'.$model->login.'.zip';
                if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
                    exit("Невозможно открыть < $filename>\n");
                }
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers.ovpn', 'uadevelopers.ovpn');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers-gateway.ovpn', 'uadevelopers-gateway.ovpn');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.pdf', $model->login. '.pdf');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.key', $model->login. '.key');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.crt', $model->login. '.crt');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/' . 'ca.crt', 'ca.crt');
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . 'files/' . 'ta.key', 'ta.key');
                //$zip->setPassword($var1);
                $zip->close();

                if($zip->status != 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . 'files/'.$model->login.'.zip')) {
                    Yii::app()->user->setFlash('tipZip','Проблема с архивацией');
                    $this->redirect('error_generate');
                }

                // Send Email




                $mail = Yii::createComponent('application.extensions.mailer.EMailer');
                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->Host = "smtp.gmail.com"; // SMTP server
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "tls";                  // enable SMTP authentication
                $mail->Port = 587;                    // set the SMTP port for the GMAIL server
                $mail->Username = "noreply@uadevelopers.com"; // SMTP account username
                $mail->Password = "noreplay24may";        // SMTP account password
                $mail->SetFrom('noreply@uadevelopers.com', 'Vpn access');
                $mail->AddReplyTo("noreply@uadevelopers.com", "VPN access");
                $mail->Subject = "VPN access for ".$model->surname." ".$model->name;
                $mail->IsHTML(true);
                if($model->id_company == 1) {
                    $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . 'files/logo-flex.gif', 'my-attach');
                } else {
                    $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . 'files/logo-ukad.gif', 'my-attach');
                }
                $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
                $body = "<html xmlns=\"http://www.w3.org/1999/html\"><br /><img style='float: left; margin: 7px 12px 7px 7px;' alt='PHPMailer' src='cid:my-attach' /><div style='font-size: 14px;'><p>Вам, <strong>$model->name $model->surname</strong>, был создан доступ к VPN серверу.<br /> В прикрепленном архиве Вы найдете конфигурационный файл и сертификаты, а также PDF инструкцию с настройками и установкой.</p><strong> Пароль на открытие PDF файла: </strong> $var1  </div></html>";

                $mail->MsgHTML($body);
                $address = $model->email;
                $copy_address = "andrey.kopitsa@flexaspect.com";
                $mail->AddAddress($address, $model->name." ".$model->surname);
                $mail->AddCC($copy_address, "Копия");
                $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . 'files/'.$model->login.'.zip');
                $mail->CharSet = "UTF-8";
                $status_mail = $mail->Send();


                // Send Email
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers.ovpn');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/uadevelopers-gateway.ovpn');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.pdf');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.key');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . $model->login . '.crt');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . 'ca.crt');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . 'ta.key');
                unlink($_SERVER['DOCUMENT_ROOT'] . 'files/'.$model->login.'.zip');

                //$this->redirect(array('view', 'id' => $model->id, 'mail' => $status_mail, 'certificate' => $retval, 'zip' => $zip->status));
                if($status_mail != 1) {
                    Yii::app()->user->setFlash('tipMail','Проблема с отправкой почты');
                    $this->redirect('error_generate');
                }



                Yii::app()->user->setFlash('tipDay','Данные сохранены');
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

    public function actionError_generate()
    {
        $dataProvider= "test";
        $this->render('error_generate',array(
            'dataProvider'=>$dataProvider,
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
