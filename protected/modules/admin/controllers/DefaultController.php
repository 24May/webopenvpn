<?php

class DefaultController extends Controller
{
    public $layout='/layouts/column2';

	public function actionIndex()
	{
        if(Yii::app()->user->checkAccess('1'))
		    $this->render('index');
        else
            $this->redirect('/site/login');
	}
}