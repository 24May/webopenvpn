<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Компании'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список компаний', 'url'=>array('index')),
	array('label'=>'Создать компанию', 'url'=>array('create')),
	array('label'=>'Изменить компанию', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить компанию', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление компаниями', 'url'=>array('admin')),
);
?>

<h1>Просмотр компании #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
