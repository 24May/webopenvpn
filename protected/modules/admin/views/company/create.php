<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Компании'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список компаний', 'url'=>array('index')),
	array('label'=>'Управление компаниями', 'url'=>array('admin')),
);




?>

<h1>Создать компанию</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>