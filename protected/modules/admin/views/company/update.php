<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Компании'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список компаний', 'url'=>array('index')),
	array('label'=>'Создать компанию', 'url'=>array('create')),
	array('label'=>'Просмотр компании', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление компаниями', 'url'=>array('admin')),
);
?>

<h1>Изменение компаний <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>