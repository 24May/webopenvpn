<?php
/* @var $this CompanyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Компании',
);

$this->menu=array(
	array('label'=>'Создать компанию', 'url'=>array('create')),
	array('label'=>'Управление компаниями', 'url'=>array('admin')),
);
?>

<h1>Компании</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
