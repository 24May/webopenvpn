<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Пользователи',
);

$this->menu=array(
    array('label'=>'Создать пользователя', 'url'=>array('create')),
    array('label'=>'Управление пользователями', 'url'=>array('admin')),
);
?>

<div class="alert alert-error">Ошибки создания сертификатов и инструкций</div>

<?php if(Yii::app()->user->hasFlash('tipPdf')):?>
    <span class="label label-important">
        <?php echo Yii::app()->user->getFlash('tipPdf'); ?>
    </span>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('tipOvpn')):?>
    <span class="label label-important">
        <?php echo Yii::app()->user->getFlash('tipOvpn'); ?>
    </span>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('tipCert')):?>
    <span class="label label-important">
        <?php echo Yii::app()->user->getFlash('tipCert'); ?>
    </span>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('tipZip')):?>
    <span class="label label-important">
        <?php echo Yii::app()->user->getFlash('tipZip'); ?>
    </span>
<?php endif; ?>