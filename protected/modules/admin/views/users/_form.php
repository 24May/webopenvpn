<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
Yii::app()->clientScript->registerScript('eneter',"
$('#Users_enter').change(function(){
    if($(this).val() == 1) {
        $('#Users_enter').parent().next('div').show();
    } else {
        $('#Users_enter').parent().next('div').hide();
    }
});
");

?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_company'); ?>

        <?php echo $form->dropDownList($model, 'id_company', Company::AllCompany()); ?>
		<?php echo $form->error($model,'id_company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'login'); ?>
		<?php echo $form->textField($model,'login',array('size'=>60,'maxlength'=>70)); ?>
		<?php echo $form->error($model,'login'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>70)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'enter'); ?>

        <?php echo $form->dropDownList($model,'enter',array(1 => 'Да',0 => 'Нет')); ?>
        <?php echo $form->error($model,'enter'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'login_password'); ?>
		<?php echo $form->textField($model,'login_password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_password'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->