<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Пользователи'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список пользователей', 'url'=>array('index')),
	array('label'=>'Создать пользователя', 'url'=>array('create')),
	array('label'=>'Изменение пользователя', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить пользователя', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление пользователями', 'url'=>array('admin')),
);
?>

<h1>Просмотр пользователя #<?php echo $model->name." ".$model->surname; ?></h1>

<?php if(Yii::app()->user->hasFlash('tipDay')):?>
    <span class="label label-success">
        <?php echo Yii::app()->user->getFlash('tipDay'); ?>
    </span>
<?php endif; ?>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_company' => array(
            'name' => 'id_company',
            'type' => 'text',
            'value' => $model->company->name,
        ),
		'name',
		'surname',
		'login',
		'email',
		'key_password',
		'login_password',
		'enter' => array(
            'name' => 'enter',
            'value' => ($model->enter == 0) ? "Нет" : "Да",
        ),
	),
)); ?>
