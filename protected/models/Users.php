<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property integer $id_company
 * @property string $name
 * @property string $surname
 * @property string $login
 * @property string $email

 * @property string $login_password
 * @property string $enter
 */
class Users extends CActiveRecord
{
    const ROLE_ADMIN = 'administrator';

    const ROLE_USER = 'user';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_company, name, surname, login, email,  enter', 'required'),
			array('id_company', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>20),
			array('surname', 'length', 'max'=>25),
			array('login, email', 'length', 'max'=>70),
			array('login_password', 'length', 'max'=>255),
			array('enter', 'length', 'max'=>1),
            array('email', 'email', 'checkMX'=> true),
            array('email', 'validateOurDomain'),
            //array('email', 'match', 'pattern' => '/^[a-zA-Z0-9._-]+@flexaspect.com$/', 'message' => 'Only Flexaspect'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_company, name, surname, login, email,  login_password, enter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'company' => array(self::BELONGS_TO, 'Company', 'id_company'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_company' => 'Компания',
			'name' => 'Имя',
			'surname' => 'Фамилия',
			'login' => 'Логин',
			'email' => 'Email',

			'login_password' => 'Пароль на вход',
			'enter' => 'Авторизация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('email',$this->email,true);

		$criteria->compare('login_password',$this->login_password,true);
		$criteria->compare('enter',$this->enter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function validateOurDomain($attribute) {
        $flexaspect = '/^[a-zA-Z0-9._-]+@flexaspect.com$/';
        $ukad = '/^[a-zA-Z0-9._-]+@ukad-group.com$/';
        if(!preg_match($flexaspect, $this->email) && !preg_match($ukad, $this->email)) {
            $this->addError($attribute, 'Доступны только домены FlexAspect или Ukad');
        }
    }

/*    public function beforeSave() {
        if($this->isNewRecord) {
            $count_ch = 15; // количество сиволов в пароле
            $var1 = '';
            $accepted = '0987654321zyxwvutsrqponmlkjihgfedcbaQWERTYUIOPASDFGHJKLMNBVCXZ';  // доступный набор символов в пароле
            srand(((int)((double)microtime()*1000000))); // меняем начальное число генератора случайных чисел
            for ($i=0; $i<=$count_ch; $i++) { //формируем случайный символ для каждой составляющей пароля
                $random = rand(0, (strlen($accepted) -1)); // берем случайный индекс из строки $accepted
                $var1 .= $accepted[$random] ; // дописываем в конец генерируемого пароля полученный символ
            }
            $this->key_password = $var1;
        }
        return parent::beforeSave();
    }*/

    protected function beforeDelete() {
        if(!parent::beforeDelete()) {
            return false;
        }
        $this->revokeCert($this->login);
        return true;
    }

    public function revokeCert($login) {
        $run_create_cert = "sudo /etc/openvpn/easy-rsa/my-revoke.sh"." ".$login;
        $result = system($run_create_cert, $retval);
    }

}
