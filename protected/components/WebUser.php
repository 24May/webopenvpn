<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 17.03.15
 * Time: 19:49
 */
class WebUser extends CWebUser {
    private $_model = null;

    function getRole() {
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->enter;
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = Users::model()->findByPk($this->id, array('select' => 'enter'));
        }
        return $this->_model;
    }
}
